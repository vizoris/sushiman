$(function() {


// Фиксированная шапка припрокрутке
$(window).scroll(function(){

  if ($(window).width() > 768) {
      var sticky = $('.header'),
          scroll = $(window).scrollTop();
      if (scroll > 100) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});


$(window).resize(function() {
  if ($(window).width() <= 768) {
      $('.header').removeClass('header-fixed'); 
    }

});





// Показать/скрыть корзину при наведении
$('.cart').hover(function() {
  $(this).toggleClass('active');
  $('.cart-block').stop(true, true).fadeToggle();
})






// Счетчик при клике "Добавить в корзину"
$('.product-item__bottom--btn .btn').click(function() {
  $(this).fadeOut(0);
  $(this).next('.count').addClass('active');
})





// Banner slider
  $('.banner-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed:4000,
      fade: false,
      arrows: true,
      dots: true,
      centerMode: true,
      centerPadding: 0,
      variableWidth: true,
      responsive: [{
        breakpoint: 1200,
        settings: {
            variableWidth: false,
        }
        
      }
    ]
     
  });





// shares slider
  $('.shares-slider').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed:4000,
      arrows: false,
      dots: false,
      // variableWidth: true,

      responsive: [
      {
        breakpoint: 1310,
        settings: {
            slidesToShow: 2,
        }
        
      },
      {
        breakpoint: 768,
        settings: {
            slidesToShow: 1,
        }
        
      },
    ]
     
  });





// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();



// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrap').find('#' + _id);
    $(this).parents('.tabs-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-wrap').find('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});




// С какой суммы подготовить сдачу
$('input:radio[name="payment-method"]').change(
    function(){
        if ($('#payment-cash').is(':checked')) {
            $('#give-change').fadeIn(100)
        } else {
          $('#give-change').fadeOut(100)
        }
    });




// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");




// MMENU
if ($(window).width() < 1200) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}




})